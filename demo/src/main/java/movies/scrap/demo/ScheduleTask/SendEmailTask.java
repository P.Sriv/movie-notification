package movies.scrap.demo.ScheduleTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.mail.javamail.JavaMailSender;

import movies.scrap.demo.Service.ScrapingService;

@Component
@EnableScheduling
public class SendEmailTask  {
    @Value("${spring.receiver}")
    private String to;
    @Autowired
    private ScrapingService scrapingService;
    @Autowired
    private JavaMailSender mailSender ; // binding from app.config 


    // @Scheduled(cron = "0 0 10 ? * THU") // second minute hour dateOfMonth Month dayOfWeek
    @Scheduled(fixedRate = 5000)
        public void sendEmail(){
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(to);
            mail.setSubject("noreply- upcoming movies");
            mail.setText(scrapingService.getMovies());
            mailSender.send(mail);
        }
    
}
