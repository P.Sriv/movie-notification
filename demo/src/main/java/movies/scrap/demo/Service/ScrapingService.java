package movies.scrap.demo.Service;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import movies.scrap.demo.Model.Movie;

import java.util.*;
@Service
public class ScrapingService {
        // WebDriver driver = new ChromeDriver();

        
        @Value("${spring.movie.web.target}")
        private String targetUrl;

       
        public String healtCheck(){
            return targetUrl;
        }

        public String getMovies(){
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            WebDriver driver = new ChromeDriver(chromeOptions);
            driver.get(targetUrl);
            WebElement comming = driver.findElement(By.className("comingsoon_out"));
            List<WebElement> comming_movies_date =comming.findElements(By.className("mlb-date"));
            List<WebElement>  comming_movies_name = comming.findElements(By.className("mlb-name"));
            Map<String,String> nameDate= new HashMap<>();
            for(int i =0;i<comming_movies_date.size();i++){
                if(comming_movies_name.get(i).findElement(By.tagName("a")).getText().equals("")) continue;
                nameDate.put(comming_movies_name.get(i).findElement(By.tagName("a")).getText(), comming_movies_date.get(i).getText());
            }
            driver.quit();
            
            return formatResponse(nameDate);
        }
        public String formatResponse(Map<String,String> movieMap){
            String parseToText = "";
            for(Map.Entry<String,String> entry: movieMap.entrySet() ){
                parseToText+= "Name:"+entry.getKey()+" Release Date:"+entry.getValue()+"\n";
            }
            return parseToText;
        }
}
