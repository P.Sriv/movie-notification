package movies.scrap.demo.Model;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    private String name;
    private String releaseDate;

    @Override
    public String toString(){
        return "Name:"+this.name+" Release Date:"+releaseDate;
    }
}
